FROM debian:buster-slim AS slimdown
ADD gcc-arm-none-eabi-x86_64.tar.bz2 /opt
COPY topurge.sh /opt
RUN cd /opt && ./topurge.sh

FROM debian:buster-slim
RUN apt-get update && apt-get install --no-install-recommends -y \
	cmake make \
	python3-minimal python3-git \
	&& rm -rf /var/lib/apt/lists/*
COPY --from=slimdown /opt/gcc-arm-none-eabi-10-2020-q4-major /opt/gcc-arm-none-eabi-10-2020-q4-major
RUN useradd -m build -s /bin/bash
USER build
ENV ARMGCC_DIR /opt/gcc-arm-none-eabi-10-2020-q4-major
