# LPC Firmware build container

This is a container with the required components to build
the LPC804 microcontroller firmware for the Ten64 board.

The toolchain is sourced from [developer.arm.com](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads).

To reduce the container size, unnecessary architectures (e.g anything not the 
[ARMv6-M](https://en.wikipedia.org/wiki/ARM_Cortex-M#Instruction_sets)
used by the Cortex-M0+) have been removed from the toolchain.
