du -h --max-depth=1 .
for dir in $(find -name v5te* -o -name v7* -o -name v8* -o -name doc -o -wholename '*/thumb/nofp'); do
	rm -rf "${dir}"
done
du -h --max-depth=1 .
