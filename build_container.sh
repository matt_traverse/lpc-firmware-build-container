#!/bin/sh
BUILD_ARCH=$(uname -m)
IMAGE_TAG=${IMAGE_TAG:-"lpc-firmware-build-env"}
case "${BUILD_ARCH}" in
	x86_64)
		DOWNLOAD_URL="https://developer.arm.com/-/media/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2?revision=ca0cbf9c-9de2-491c-ac48-898b5bbc0443&la=en&hash=68760A8AE66026BCF99F05AC017A6A50C6FD832A"
	;;
	aarch64)
		DOWNLOAD_URL="https://developer.arm.com/-/media/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-aarch64-linux.tar.bz2?revision=cff794bc-3fb1-4c9c-934b-782886767324&la=en&hash=BE4394386FD84320EBAE02DAC88ED8C4AB9A74FD"
	;;
	*)
		echo "Unsupported build architecture: ${BUILD_ARCH}"
		exit 1
	;;
esac
if [ ! -f gcc-arm-none-eabi-x86_64.tar.bz2 ]; then
	curl -L "${DOWNLOAD_URL}" -o gcc-arm-none-eabi-x86_64.tar.bz2
fi
sudo --preserve-env=DOCKER_HOST docker build -t "${IMAGE_TAG}" .
